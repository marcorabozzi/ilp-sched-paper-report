\documentclass[10pt]{article}

\usepackage{times}
\usepackage{amsmath}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage[utf8]{inputenc}
\usepackage{changepage}
\usepackage{lscape}
\usepackage{rotating, graphicx}
\usepackage{amsthm}
\usepackage{setspace}
\usepackage{amssymb}

\usepackage{acronym}
\acrodef{FPGA}[FPGA]{Field Programmable Gate Array}
\acrodef{CLB}[CLB]{Configurable Logic Block}
\acrodef{HDL}[HDL]{Hardware Description Language}
\acrodef{PDR}[PDR]{Partial Dynamic Reconfiguration}
\acrodef{MILP}[MILP]{Mixed-Integer Linear Programming}
\acrodef{BRAM}[BRAM]{Block RAM}
\acrodef{DSP}[DSP]{Digital Signal Processor}
\acrodef{API}[API]{Application Programming Interface}
\acrodef{TRFP}[TRFP]{Token Ring Floorplanning Problem}
\acrodef{GA}[GA]{Genetic Algorithm}
\acrodef{TSP}[TSP]{Traveling Salesman Problem}
\acrodef{HPWL}[HPWL]{Half-Perimeter Wire Length}


\newcommand{\eg}{\textit{e.g.}~}
\newcommand{\ie}{\textit{i.e.}~}

\oddsidemargin 1.1cm
\textwidth 14.5cm

\title{\large ILP and Scheduling. Project report. \\
{\Large Floorplanning of a token ring topology \\ on Partially-Reconfigurable FPGAs}}

\author
{Marco Rabozzi\\
\footnotesize{Politecnico di Milano}\\
\footnotesize{marco.rabozzi@polimi.it}
}

\date{}

%%%%%%%%%%%%%%%%% END OF PREAMBLE %%%%%%%%%%%%%%%%


\begin{document} 

\baselineskip13pt
\maketitle 


\setlength\abovedisplayskip{3pt}
\setlength\belowdisplayskip{3pt}


\section{Introduction}\label{intro}
\acp{FPGA} are widely employed in commercial and industrial applications in many fields such as automotive, video and image processing, bio-engineering, finance, big-data analytics and high performance computing, to cite a few. The adoption of such devices is mainly motivated by their high power efficiency, reduced cost and high flexibility, since they can be reconfigured as needed by the system designer to change their functionality. From a high level perspective, \acp{FPGA} are composed of basic \acp{CLB},
that allow implementing simple sequential and combinatorial logic (\eg AND / OR circuits and registers), connected by means of configurable interconnect tiles. The configuration of both \acp{CLB} and interconnect tiles allows the designer to define an arbitrary circuit without incurring the cost of creating or purchasing custom hardware. Furthermore, recent \acp{FPGA} also provide other types of reconfigurable resources, such as \ac{DSP} and \ac{BRAM} that enable achieving higher performance while reducing the application resource requirements.

%The conventional flow to develop an application for an \ac{FPGA} device consists of five major steps: 
%\begin{enumerate}
%\item hardware specification: the designer specifies the description of the functionality by means of \ac{HDL} such as VHDL or Verilog
%\item synthesis: the \ac{HDL} is synthesized on the target \ac{FPGA} and the high level description given by the \ac{HDL} is converted into a netlist, after this phase an estimate of the resources required by the application on the \ac{FPGA} is available
%\item place and route: the elements within the netlist are configured (placed) on specific \ac{FPGA} components (e.g. \acp{CLB}) and routing among such components is defined
%\item bitstream generation: starting form the place and routed netlist, a bitstream file is generated containing the configuration data for all the \ac{FPGA} components
%\item configuration: the bitstream file is loaded into the \ac{FPGA} configuration memory and the application is ready to operate
%\end{enumerate}

Within the conventional \ac{FPGA} design flow, the device is configured as a whole and every time a reconfiguration is required, the currently running application must be interrupted. Recently, \ac{FPGA} vendors such as Xilinx enabled the capability to perform \ac{PDR} \cite{pr}, that is the possibility to partially reconfigure a region of the \ac{FPGA} while the portion of the device not affected by reconfiguration can still operate. \ac{PDR} enables usage scenarios in which an application (or multiple applications) can be subdivided into separate sub-tasks that are partially reconfigured and executed over time onto the device. This is especially useful when an application does not entirely fit in the reconfigurable logic or when the sub-tasks to be executed are known only at runtime. Nevertheless, designing such a partially reconfigurable system is a complex task that requires to define an appropriate communication infrastructure and the floorplanning \cite{wong} of reconfigurable regions. A reconfigurable region represents a placeholder for hosting multiple reconfigurable modules, while floorplanning refers to the assignment of a reconfigurable region to a specific area onto the \ac{FPGA} resource grid. Within this context, the Dyplo framework realized by Topic \cite{dyplo} greatly simplifies the development of a partially reconfigurable system. Indeed, Dyplo provides both a token ring communication infrastructure, in which the reconfigurable regions are connected in a loop, and a set of \acp{API} that ease the reconfiguration of the modules. 
However, within the proposed framework, the designer still has the burden of manually performing the floorplanning, at the risk of obtaining suboptimal routing performance. More precisely, the longest connection among two reconfigurable regions within the interconnection loop constrains the circuit clock frequency and directly impacts on the communication delay.

In the classical floorplanning problem for partially-reconfigurable \acp{FPGA}, the interconnections among the reconfigurable regions are assigned a priori before floorplanning \cite{rabox2015, tvlsi}, whereas, when considering the Dyplo framework, the floorplanner is also allowed to specify the order in which the reconfigurable regions are connected in the token ring topology. With this work, we evaluate the performance gain that can be achieved by leveraging this additional degree of freedom. In particular, we devise a compact \ac{MILP} formulation for the \ac{TRFP} and propose a \ac{GA} metaheuristic enhanced with a local search strategy on the region placements, as well as a simple variant of the classical \ac{TSP} 2-opt heuristic for the interconnections order.

\section{Problem definition}\label{problem}

In the first part of this section, we provide some relevant background on the floorplanning problem, in particular focusing on the Xilinx \ac{FPGA} devices and the design rules of the related \ac{PDR} flow, whereas, in the second part of the section, we describe the details related to the \ac{TRFP} variant. Notice that the first subsection borrows most of the notation and \ac{PDR} constraints definition from our previous work \cite{tvlsi}, these information are reported here in order to provide a self-contained discussion.

\subsection{FPGA device model and floorplanning constraints}

As shown in Figure~\ref{fig:fpga}a, the reconfigurable fabric of an \ac{FPGA} device is organized in a set of columns of resources of various types, that is $T=\{CLB,BRAM,DSP\}$. The grid is also organized in quadrants, called \textit{clock regions} according to the structure of the clock tree and the organization of the configuration memory. Moreover, based on this last one, the basic reconfiguration portion of the device grid, that we call \textit{tile}, spans one clock region height and one resource width. Each tile contains a single type of resource depending on the position of the tile, and the amount of units depends on the type of resource. Finally, we also define a coordinate system on the grid of tiles, starting from the left-bottom corner; we denote with $W$ and $H$ the maximum values on the $X$ and $Y$ axis, respectively.

%In order to use \ac{PDR}, it is necessary to partition the system design into different components among which a of reconfigurable regions $N$ can be defined. For every reconfigurable region $n \in N$, the designer has to specify a region placement, that is a rectangular area spanning multiple resources onto the \ac{FPGA} resource grid. In figure \ref{fig:fpga}, we show the top level design of an application together with an example of an \ac{FPGA} device model and the assignment of reconfigurable regions on top of the \ac{FPGA}. Every reconfigurable region can host several modules, and, for any given time instant, only a single module can be reconfigured within a specific reconfigurable region if the corresponding region placement provides enough resources. Furthermore, recent \acp{FPGA} devices also contain other types of configurable resources such as \ac{BRAM} and \ac{DSP} that can be leveraged by the application. Hence, the resource requirements for the reconfigurable modules are in general multi-dimensional.

\begin{figure}[h!tb]
\centering
    \includegraphics[width=0.7\linewidth]{img/fpga}
    \caption{Problem representation in terms of a) the reconfigurable \ac{FPGA} device model and b) the top level structural description of the system.}
    \label{fig:fpga}
\end{figure}  

According to the \ac{PDR} design guidelines, as show in Figure~\ref{fig:fpga}b, the reconfigurable system is specified in terms of a structural description of interconnected top components, among which a set of $N$ \textit{reconfigurable regions} are defined. Each region implements a partially-reconfigurable unit in which it will be possible to load a set of different modules implementing different functionalities. Thus, the reconfigurable region $n \in N$ presents resource requirements that depends on the modules that the designer is willing to host; for each resource type $t \in T$ we denote the required amount as $r_{n,t}$.
Moreover, the region is connected with the others and with the static part of the design (another component or set of components not featuring reconfiguration capabilities) by means of a set of interconnection buses.

The goal of the floorplanning is to define a \textit{placement} for each of the reconfigurable regions, in terms of rectangular shape and position on the \ac{FPGA} resource grid.
To this purpose, on the basis of the defined \ac{FPGA} model, we denote with $P$ the set of all possible placements that may be defined for the floorplanning of a single reconfigurable region:
\begin{equation}
\begin{split}
P = \{(x,y,w,h) \mid~& x,y,w,h \in \mathbb{N}, \\
 & x+w \leq W, y+h \leq H \}
\end{split}
\end{equation}
where $x$ and $y$ represent the coordinates of the bottom-left corner of the placement, while $w$ and $h$ define its width and height respectively. Thus, the specific placement $p$ can be characterized in terms of the available resource capacity, denoted as $c_{p,t}$ (for each resource type $t$), depending on the specific position and shape.
It is worth noting that in some \acp{FPGA} a specific subset of the placements $S \subset P$ are forbidden since they overlap with hard processors, static logic or I/O block. For a formal description of the floorplanning requirements it is convenient to define a relation $\perp$ such that for $p_{1},p_{2} \in P$: $p_{1} \perp p_{2}$ if and only if the two placements overlap on at least a tile. The non-overlapping relation $\not\perp$ is simply defined as the complement of $\perp$: $\not\perp = P \times P \thinspace \setminus \perp$.

To be feasible, a floorplan must assign a placement $p_n \in P$ for each region $n \in N$ and satisfy a set of \ac{PDR} requirements:

\begin{description}
\item[REQ1:] each assigned placement must contain at least the required resources for the corresponding region:
\begin{equation}
\forall n \in N, t \in T:  c_{p_n,t} \geq r_{n,t} 
\end{equation}
\item[REQ2:] each assigned placement must not be forbidden:
\begin{equation}
\forall n \in N: p_n \not\in S
\end{equation}
\item[REQ3:] the left and right boundaries of a placement $p_n$ must be aligned to specific coordinates that prevent splitting of interconnect resources \cite{pr} ($VL$ and $VR$ enumerate valid left and right coordinates, respectively):
\begin{equation}
\forall p_n = (x,y,w,h) \mid n \in N: x \in VL \wedge x + w \in VR
\end{equation}
\item[REQ4:] \ac{CLB} resources at both sides of the center clock column must lie in the static part of the design, an assigned placement can cross the center column but such resources are not available for the corresponding region:
\begin{equation}
\forall t \in T: c_{(x_{clk} - 1, 0, 2, H),t} = 0
\end{equation}
\item[REQ5:] placements assigned to two different regions cannot overlap:
\begin{equation}
\forall p_{n1},p_{n2} \mid n1,n2 \in N \wedge n1 \neq n2: p_{n1} \not\perp p_{n2}
\end{equation}
\end{description}
This list of constraints can be partitioned in two groups: REQ1-REQ4 are specifically related to the placement $p_n$ for a single region $n$, while REQ5 rules the relative positions between different regions. Moreover, the first set can be summarized in a single definition by introducing a new set $P_n$, which represents all the feasible placements on the device for a reconfigurable region~$n$.

In conclusion, the floorplanning problem can be stated as follows: \textit{Given the sets $P_{n}$ of feasible placements, a floorplan is a function $f$ that assigns for each region $n \in N$ a placement $p \in P_{n}$ such that there is no overlapping among the placements}. More formally:
\begin{equation}
\begin{split}
&f: n \in N \rightarrow p \in P_n\\
&f(n_1) \not\perp f(n_2) \enspace \forall n_1,n_2 \in N: n_1 \neq n_2 \label{eq:floorplanFunction}
\end{split}
\end{equation}


\subsection{Token ring floorplanning problem}

When dealing with the \ac{TRFP}, we also need to specify the interconnections between the reconfigurable regions of the design. In particular, other than the floorplan function $f$, the problem requires to identify an undirected interconnection graph $G = (N,E)$ on the regions $N$, so that the edges $E$ form an Hamiltonian cycle. Each connection $e = (n1,n2) \in E$ has a wire length $l_e$ that depends on the position of the reconfigurable regions $n1$ and $n2$. Within this context, a common wire length estimation is the \ac{HPWL} that considers the interconnection pins to be located at the center of the regions and measures the wire length using the Manhattan distance. As an example, figure \ref{fig:connections} shows the floorplan of 5 reconfigurable regions together with the corresponding interconnections. In order to have a good estimate of the wire length, we take into account the dimensions of a tile within the \ac{FPGA} resource grid. For this purpose, we denote by $tileW$ and $tileH$ the width and the height of a tile respectively, while the coordinates $(cpx_p, cpy_p)$ of the center of a placement $p = (x,y,w,h) \in P$ are computed as: 
%
\begin{equation}
cpx_p = (x + w/2) \cdot tileW, \quad cpy_p = (y + h/2) \cdot tileH
\end{equation}
For a given floorplan function $f$, the wire length $l_{n1,n2}$ of a connection $(n1,n2) \in E$ is computed using the Manhattan distance among the center of the regions:
\begin{equation}
l_{n1,n2} = |cpx_{f(n1)} - cpx_{f(n2)}| + |cpy_{f(n1)} - cpy_{f(n2)}|
\end{equation}

In most of the cases, from an optimization point of view, it is not efficient to consider region placements larger than the minimal bounding boxes containing all the required resources. In fact, as discussed in~\cite{wong} and \cite{bolchini}, by using the minimal bounding boxes it is possible to reduce both resource utilization, thus leaving space for additional functionalities, and the reconfiguration time.
For this reason, we define a new set $P^{irr}_n \subseteq P_n$ containing the \textit{irreducible placements} of a region $n$ as:
\begin{equation}
P^{irr}_n = \{p \in P_n \mid \nexists p_2 \in P_n: p_2 \neq p \wedge p_2 \prec p \}
\end{equation}
where $\prec$ represents a containment relation between two different placements of the same region. More formally, given two placements $p_1 = (x_1,y_1,w_1,h_1),p_2 =(x_2,y_2,w_2,h_2)\in P_n$,  we have $p_1 \prec p_2$ if and only if $x_1 \geq x_2$, $y_1 \geq y_2$, $x_1+w_1 \leq x_2 + w_2$ and $y_1+h_1 \leq y_2+h_2$.
Additionally, by considering $P^{irr}_n$ instead of $P_n$ we considerably reduce the size of the solution space and we can easily enumerate all the elements in $P^{irr}_n$ for a given region $n$. Indeed, the following upper bound holds for the size of $P^{irr}_n$:
%
\begin{equation}
|P^{irr}_n| \leq \min \{W^2 \cdot H , \ H^2 \cdot W\}
\end{equation}
%
In practice, a large \ac{FPGA} such as the Xilinx XC7v585tff6 has $W=123$ and $H=9$, while $|P^{irr}_n|$ is rarely greater than 1500 due to the additional constraints on forbidden placements and horizontal placements alignment.
To summarize, the \ac{TRFP} consists in finding a feasible floorplan $f: n \in N \rightarrow p \in P^{irr}_n$ and an Hamiltonian graph $G = (N,E)$ so that the longest connection is minimized:
%
\begin{equation}
\min \left\{ \max_{(n1,n2) \in E} l_{n1,n2} \right\}
\end{equation}


\begin{figure}[h!tb]
\centering
    \includegraphics[width=0.7\linewidth]{img/connections}
    \caption{Example of a floorplan using a token ring interconnect topology.}
    \label{fig:connections}
\end{figure}  


\section{MILP formulation}\label{milp}

Within this section we extend and modify the formulation presented in our previous work \cite{rabox2015} to deal with the additional degree of freedom offered by the \ac{TRFP}. In order to obtain a compact formulation, the interconnections among the reconfigurable regions are considered as directed arcs and the L2ATSPxy formulation \cite{atsp} has been integrated in the model to ensure regions connectivity. The model parameters, sets and variable are summarized in Table \ref{tab:parameters}, whereas Table \ref{tab:constraints} lists the set of constraints and defines the objective function. The main decision variables of the problem are $x_{n,p}$ and $e_{n1,n2}$ that control the placements and the connections of the reconfigurable regions respectively. In particular, $x_{n,p}$ states whether region $n \in N$ is assigned to placement $p \in P^{irr}_n$, whereas $e_{n1,n2}$ is set to 1 if and only if a connection from $n1 \in N$ to $n2 \in N$ is established. The non overlapping of reconfigurable regions is ensured by constraints C2 that allows to select only one placement among all the ones that overlap on at least a tile. From a different perspective, this type of constraint is equivalent to a clique cut for the maximum independent set problem, in which nodes are represented by regions placements and edges are inserted for each couple of overlapping placements. In order to compute the wire length among the regions, we introduce the support variables $cx_n, cy_n$ representing the center of a reconfigurable region $n \in N$. Moreover, we define the variables $dx_{n1,n2}, dy_{n1,n2}$ to take into account the horizontal and vertical distance among regions $n1, n2 \in N$. The semantics of the previous variables is guaranteed by the presence of constraints C3-C8 that assign the center of a region to the selected placement and provide lower bounds on the distance among regions centers.

\begin{table}[h!]
\centering
\caption{MILP variables, sets and parameters\label{tab:parameters}}
\resizebox{\columnwidth}{!}{
\begin{tabular}{|l|l|}
\hline
\multicolumn{2}{|l|}{\textbf{Sets}}\\
\hline
$N$ & set of reconfigurable regions to floorplan\\[+.05in]
$P^{irr}_n$ & set of irreducible feasible placements for region $n \in N$\\[+.05in]
\hline
\multicolumn{2}{|l|}{\textbf{Parameters}}\\ 
\hline
$W$ & maximum value on the horizontal direction\\[+.05in]
$H$ & maximum value on the vertical direction\\[+.05in]
$tileW$ & the width of a tile within the \ac{FPGA}\\[+.05in]
$tileH$ & the height of a tile within the \ac{FPGA}\\[+.05in]
$maxL$ & upper bound on the longest connection\\[+.05in]
$n0$ & an arbitrary reference region in $N$\\[+.05in]
\hline
\multicolumn{2}{|l|}{\textbf{Variables}}\\ 
\hline
$x_{n,p}$ & binary variable set to 1 if and only if the placement $p \in P^{w}_{n}$ is selected for region $n \in N$\\[+.05in]
$e_{n1,n2}$ & binary variable set to 1 if there is a connection from region $n1 \in N$ to $n2 \in N$\\[+.05in]
$y_{n1,n2}$ & binary variable set to 1 if there is a directed path from region $n1 \in N$ to $n2 \in N$ not crossing $n0$\\[+.05in]
$cx_{n}$ & real non negative $x$ coordinate of region $n \in N$ center\\[+.05in]
$cy_{n}$ & real non negative $y$ coordinate of region $n \in N$ center\\[+.05in]
$dx_{n1,n2}$ & real non negative horizontal distance between centers of regions $n1, n2 \in N$\\[+.05in]
$dy_{n1,n2}$ & real non negative vertical distance between centers of regions $n1, n2 \in N$\\[+.05in]
$l_{max}$ & real non negative wire length of the longest connection \\[+.05in]
\hline
\end{tabular}
}
\end{table}

\begin{table}[h!]
\centering
\caption{MILP model constraints and objective function.\label{tab:constraints}}
\resizebox{\columnwidth}{!}{
\begin{tabular}{|l|l|}
\hline
\multicolumn{2}{|l|}{\textbf{Placements constraints}}\\
\hline
C1 & $\sum_{p \in P^{irr}_{n}} x_{n,p} = 1, \forall n \in N$\\[+.05in]
C2 & $\sum_{n \in N, p \in P^{irr}_{n}: p \perp (xp,yp,1,1)} x_{n,p} \leq 1, \forall xp \in [0,W-1], yp \in [0,H-1]$\\[+.05in]
\hline
\multicolumn{2}{|l|}{\textbf{Wire length semantic}}\\ 
\hline
C3 & $cx_n = \sum_{p \in P^{irr}_{n}} x_{n,p} \cdot cpx_p, \forall n \in N$\\[+.05in]
C4 & $cy_n = \sum_{p \in P^{irr}_{n}} x_{n,p} \cdot cpy_p, \forall n \in N$\\[+.05in]
C5 & $dx_{n1,n2} \geq cx_{n1} - cx_{n2}, \forall n1,n2 \in N \mid n1 \neq n2 $\\[+.05in]
C6 & $dx_{n1,n2} \geq cx_{n2} - cx_{n1}, \forall n1,n2 \in N \mid n1 \neq n2 $\\[+.05in]
C7 & $dy_{n1,n2} \geq cy_{n1} - cy_{n2}, \forall n1,n2 \in N \mid n1 \neq n2 $\\[+.05in]
C8 & $dy_{n1,n2} \geq cy_{n2} - cy_{n1}, \forall n1,n2 \in N \mid n1 \neq n2 $\\[+.05in]
\hline
\multicolumn{2}{|l|}{\textbf{L2ATSPxy constraints \cite{atsp}}}\\
\hline
C9 & $\sum_{n1,n2 \in N \mid n1 \neq n2} e_{n1,n2} = 1, \forall n1 \in N$\\[+.05in]
C10 & $\sum_{n1,n2 \in N \mid n1 \neq n2} e_{n1,n2} = 1, \forall n2 \in N$\\[+.05in]
C11 & $y_{n1,n2} \geq e_{n1,n2}, \forall n1,n2 \in N \setminus \{n0\} \mid n1 \neq n2$\\[+.05in]
C12 & $e_{n1,n2} + y_{n2,n3} + e_{n3,n2} + y_{n3,n1} + e_{n3,n1} \leq 2, \forall n1,n2,n3 \in N \setminus \{n0\} \mid n1 \neq n2
 \neq n3$ \quad \quad \quad \quad \quad \\[+.05in]
C13 & $y_{n1,n2} + y_{n2,n1} = 1, \forall n1,n2 \in N \setminus \{n0\} \mid n1 \neq n2$\\[+.05in]
\hline
\multicolumn{2}{|l|}{\textbf{Cost function definition}}\\
\hline
C14 & $l_{max} \geq dx_{n1,n2} + dy_{n1,n2} - (1 - e_{n1,n2}) \cdot maxL, \forall n1,n2 \in N \mid n1 \neq n2$\\[+.05in]
\hline
\multicolumn{2}{|l|}{\textbf{Objective function}}\\
\hline
OBJ & $\min \left\{ l_{max} \right\}$\\[+.05in]
\hline
\end{tabular}
}
\end{table}

The support variables $y_{n1,n2}$ are introduced to avoid sub cycles within the interconnect topology of the reconfigurable regions. Specifically $y_{n1,n2}$ are defined on the subset of nodes $N \setminus \{n0\}$ and state whether there exists a direct path from region $n1$ and region $n2$. The semantics of $y_{n1,n2}$ is given by constraints C11 and C12. C11 ensures that if the direct connection $e_{n1,n2}$ is enabled, then there exists a path from $n1$ to $n2$ ($y_{n1,n2} = 1$). On the other hand, C12 is a lifted version of the following constraint:
\begin{equation}\label{eq:notlifted}
y_{n1,n2} + e_{n2,n3} \leq y_{n1,n3} + 1, \forall n1,n2,n3 \in N \setminus \{n0\} \mid n1 \neq n2 \neq n3
\end{equation}
The inequality \ref{eq:notlifted} propagates the reachability semantic of variable $y_{n1,n2}$ to paths of increasing length. Indeed, if there exists a path from $n1$ to $n2$ ($y_{n1,n2} = 1$) and arc $(n2,n3)$ is selected ($e_{n2,n3} = 1$), then, it is possible to reach $n3$ from $n1$ ($y_{n1,n3} = 1$). Finally, constraints C13 removes cycles within the subgraph consisting of the nodes $N \setminus \{n0\}$. To conclude the model, the computation of the longest connection is performed by constraint C14, where the big-M constants $maxL$ is used to linearize the products among $dx_{n1,n2}, dy_{n1,n2}$ and $e_{n1,n2}$.

\section{Genetic algorithm}\label{ga}

As we will show in the result section, the \ac{MILP} based floorplanner is able to achieve the optimal solution in a relatively short amount of time for small problem instances, however, when scaling to bigger instances, the solver performance decreases drastically. For this reason, we also propose an alternative genetic algorithm metaheuristic enhanced with local search. The adopted solution encoding consists of a vector of $2|N|$ elements of the form:
%
\begin{equation}
<<p_1,p_2, \ldots, p_N>, \ <r_1, r_2, \ldots, r_N>>
\end{equation}
%
The first $|N|$ elements $p_1 \in P^{irr}_{n_1},p_2 \in P^{irr}_{n_2}, \ldots, p_N \in P^{irr}_{n_N}$ store the selected placements for the reconfigurable regions $n_1, n_2 \ldots n_N$. Instead, the second half of the vector stores a set of random numbers $r_1, r_2 \ldots, r_N$ that when sorted produce the list  $r_{a1}, r_{a2}, \ldots, r_{a_N}$ whose indexes $a_1, a_2, \ldots, a_N$ give the order in which the reconfigurable regions are connected in the token ring topology:
%
\begin{equation}
n_{a_1} \rightarrow n_{a_2} \rightarrow \cdots \rightarrow n_{a_N} \rightarrow n_{a_1}
\end{equation}

The crossover among two parent solutions is performed by splitting the list of placements at a random point and recombining the obtained parts into the two children solutions, whereas, the set of random numbers describing the token ring topology are assigned randomly to either one child or the other as shown in Figure \ref{fig:crossover}. On the other hand the mutation function, when applied, simply changes the placement of a reconfigurable region and regenerates a random number with probability $1/|N|$.

\begin{figure}[h!]
\centering
    \includegraphics[width=0.55\linewidth]{img/crossover}
    \caption{Example of a crossover operation.}
    \label{fig:crossover}
\end{figure}  


Even though the solution encoding allows to easily define the crossover and the mutation operators, it can also represents many infeasible solutions where multiple regions placements might overlap. In order to guide the exploration of the algorithm even within the infeasible region, we consider the following objective function enhanced with a penalty term $\phi$ that represents the number of couples of overlapping regions:
\begin{equation}
obj = \frac{l_{max}}{maxL} + \phi
\end{equation}
%
Furthermore, In order to enhance the convergence of the genetic algorithm, after each iteration, the newly generated solutions are improved by iteratively applying a placement local search and a variation of the 2-opt \ac{TSP} heuristic until no further improvement can be achieved. The placement local search uses only a single type of move that consists in changing the placement of a region. The heuristic iterates over the reconfigurable regions and, for each of them, selects the placement that gives the highest objective function improvement (if any). This local search, coupled with the penalty cost in the objective function, greatly improves the number of feasible solutions explored.
It is worth noting that even if the standard 2-opt heuristic is designed to reduce the overall length of a Hamiltonian cycle, there might be situations in which the longest connection increases. To avoid these cases, we use a modified 2-opt heuristic that substitutes edges $e1 = (a,b), e2 = (c,d) \in E$  with $e1' = (a,c), e2' = (b,d)$ if:
\begin{equation}
l_{e1'} + l_{e2'} < l_{e1} + l_{e2} \ \wedge \  l_{e1'}, l_{e2'} \leq max(l_{e1}, l_{e2}) 
\end{equation}


\section{Experimental results}\label{results}

Within the next section, we provide an evaluation of the proposed \ac{MILP} formulation and metaheuristic on a set of randomly generated problem instances. All the tests have been performed on a 2.5 Ghz Intel Core i7, the metaheurisitc has been implemented in C using GAlib \cite{galib}, while we used Gurobi \cite{gurobi} for solving the \ac{MILP} models. Regarding the \ac{GA}, we used a population size of 10 elements and we set the crossover and mutation probability to $85\%$ and $15\%$ respectively. Furthermore a Python script has also been implemented to generate the set of irreducible placements staring form the regions resource requirements and the \ac{FPGA} description.

As a first test, we evaluated the benefits that can be achieved when the additional degree of freedom on the interconnections among regions is given. To this purpose, we compared the quality of the solutions achieved by \cite{rabox2015}, in which the token ring topology is fixed, to the proposed \ac{MILP} formulation and metaheurisitc.
Specifically, we considered 4 small instances with 5 reconfigurable regions having an overall device occupation in the range $[70\%,75\%,80\%,85\%]$. Table \ref{tab:smalltests} reports the longest path and the time required by the algorithms to obtain the optimal solution (notice that \ac{GA} optimality has been proved by comparing its solutions to the \ac{MILP} ones). As we can see from the table, even for these small instances, both the \ac{MILP} and the \ac{GA} approaches are able to reduce the longest path by 12\% on average thanks to the added routing flexibility. Furthermore, the time required by \ac{GA} to obtain the optimal solution is from one to two order of magnitude smaller than the time needed by the \ac{MILP} solver.

\setlength{\tabcolsep}{1.5pt}
\begin{table}[tb]
\centering
\caption{Algorithms comparison on problem instances with 5 reconfigurable regions.}
\label{tab:smalltests}
\begin{tabular}{c@{\hskip 0.8cm}c@{\hskip 0.8cm}c@{\hskip 0.8cm}c@{\hskip 0.8cm}c@{\hskip 0.8cm}c@{\hskip 0.8cm}c}
\toprule
\multirow{2}{*}{Resource usage} & \multicolumn{ 3}{c}{Longest connection} & \multicolumn{ 3}{c}{Time to optimum [s]} \\
	&	\cite{rabox2015}	&	MILP	&	GA	&	\cite{rabox2015}	&	MILP	&	GA	\\
\midrule
70\%	&	84		&	80		&	80		& 	19.3	0&	192.74	&	0.44	\\
75\%	&	108		&	81		&	81		& 	23.03	&	134.19	&	1.48\\
80\%	&	183		&	175		&	175		&	10.26	&	65.14	&	0.94\\
85\%	&	230		&	198		&	198		&	9.96	&	12.61	&	1.74\\
\midrule
\bottomrule
\end{tabular}
\end{table}

As a second test, we considered a larger benchmark consisting of 20 instances in which we also varied the number of reconfigurable regions in the range $[7,14,21,28]$. All the algorithms were executed with a time budget of 300s and we observed that the \ac{MILP} approaches were never able to prove optimality within the given time limit. As we can see from the results in Table \ref{tab:bigTests}, the \ac{GA} greatly outperforms the other approaches in terms of solution quality, especially for large problems. Interestingly, \cite{rabox2015} provided better results than the presented \ac{MILP} approach in most of the instances with 14 or more regions. Indeed, even if the \ac{TRFP} \ac{MILP} model has a higher degree of freedom than the one of \cite{rabox2015}, the additional constraints required for the token ring topology increase the complexity of the model and the time required to find good solutions.


\setlength{\tabcolsep}{1.5pt}
\begin{table}[tb]
\centering
\caption{Algorithms comparison with a limited time budget of 300 seconds on 20 problem instances.}
\label{tab:bigTests}
\begin{tabular}{c@{\hskip 0.8cm}c@{\hskip 0.8cm}c@{\hskip 0.8cm}c@{\hskip 0.8cm}c@{\hskip 0.8cm}cc}
\toprule
\multirow{2}{*}{\# Regions} & \multirow{2}{*}{Algorithm} & \multicolumn{ 4}{c}{Resource Usage} \\
& 	& 70\% & 75\% & 80\% & 85\%  \\
\midrule
\multirow{3}{*}{7} 		&\cite{rabox2015}		&62	&70	&69	&192\\
						&MILP		&50	&58	&68	&165\\
						&GA		&50	&62	&68	&150\\
\midrule
\multirow{3}{*}{14} 		&\cite{rabox2015}		&100	&106	&144	&99 \\
						&MILP		&116	&133	&125	&127\\
						&GA 		&88	&87	&93	&92\\
\midrule
\multirow{3}{*}{21} 		&\cite{rabox2015}		&134	&175	&158	&183\\
						&MILP		&130	&125	&152	&282\\
						&GA 		&76	&81	&78	&82\\
\midrule
\multirow{3}{*}{28} 		&\cite{rabox2015}		&170	&198	&258	&226\\
						&MILP		&252	&388	&358	&503\\
						&GA 		&69	&75	&80	&76\\
\midrule
\bottomrule
\end{tabular}
\end{table}


\section{Conclusions}\label{conclusions}

Within this work we introduced the Token Ring Floorplanning Problem (TRFP) and presented a \ac{MILP} and a \ac{GA} based floorplanner. As demonstrated within the experimental section, the additional flexibility given to the floorplanner on the regions routing allows to reduce the longest connection among regions, hence potentially increasing the maximum clock frequency of the design. Future work will investigate the possibility to improve the local search techniques employed within the \ac{GA} and to extend the proposed approach to floorplan also static modules.


{\small
\bibliography{report}
\bibliographystyle{IEEEtran}}
\clearpage
\end{document}